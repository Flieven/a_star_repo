﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoLogic : MonoBehaviour
{
    [SerializeField] private Color GizmoColor = Color.white;
    [SerializeField] private float GizmoSize = 0.1f;

    private void OnDrawGizmos()
    {
        Gizmos.color = GizmoColor;
        Gizmos.DrawWireSphere(transform.position, GizmoSize);
    }

}
