﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMasterLogic : MonoBehaviour
{
    [SerializeField] private Vector2 gridSize = Vector2.zero;
    [SerializeField] private GameObject gridTile = null;
    private Vector3 spawnPos = Vector3.zero;
    [SerializeField] private List<GameObject> gridList = new List<GameObject>();
    [SerializeField] private GameObject gridParent = null;
    [SerializeField] private List<GameObject> startPointList = new List<GameObject>();
    [SerializeField] private List<GameObject> endPointList = new List<GameObject>();

    [SerializeField] private GameObject PathFinder = null;
    [SerializeField] private int ExtraTileCost = 2;
    private GameObject spawnedPathFinder = null;

    private void Awake()
    {
        spawnPos = transform.position;
        generateGrid();
    }

    private void Update()
    {
        CheckForMouseClicks();

        if(spawnedPathFinder == null && Input.GetKeyDown(KeyCode.E))
        { spawnPathFinder(); }
    }

    private void generateGrid()
    {
        int index = 0;
        for(int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                GameObject tempObj = Instantiate(gridTile, new Vector3(spawnPos.x + x, spawnPos.y + y, 0.1f), Quaternion.identity);
                tempObj.GetComponent<TileLogic>().setIndex(index);
                tempObj.name = "G" + index;

                int randNum = Random.Range(0, 5);

                if(randNum == 3) { tempObj.GetComponent<TileLogic>().setWall(true); tempObj.tag = "WallTile"; }
                //else if(randNum >= 6) { tempObj.GetComponent<TileLogic>().setTileModifier(ExtraTileCost); tempObj.GetComponent<SpriteRenderer>().color = Color.cyan; }
                else { tempObj.GetComponent<TileLogic>().setWall(false); }
                gridList.Add(tempObj);
                tempObj.transform.parent = gridParent.transform;
                index++;
            }
        }

        foreach(GameObject tile in gridList)
        {
            tile.GetComponent<TileLogic>().setNeighbours(getNeighbours(tile.GetComponent<TileLogic>().getIndex()));
        }

    }

    private void spawnPathFinder()
    {
        spawnedPathFinder = Instantiate(PathFinder, startPointList[0].transform.position, Quaternion.identity);
        spawnedPathFinder.GetComponent<PathFinderLogic>().giveStartData(startPointList[0], endPointList[0]);
    }

    void CheckForMouseClicks()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

        if (Input.GetMouseButtonDown(0))
        {

            if(hit.collider != null)
            {
                if (hit.collider.transform.tag == "GridTile")
                {
                    //Debug.Log("GridTile hit!");
                    hit.collider.gameObject.GetComponent<TileLogic>().setStartPos();
                }
                else if(hit.collider.transform.tag == "WallTile") { }
                else { hit.collider.gameObject.GetComponent<TileLogic>().setNormalTile(); }
            }
            updateTileLists();

        }
        else if (Input.GetMouseButtonDown(1))
        {
            if (hit.collider != null)
            {
                if (hit.collider.transform.tag == "GridTile")
                {
                    //Debug.Log("GridTile hit!");
                    hit.collider.gameObject.GetComponent<TileLogic>().setEndPos();
                }
                else if (hit.collider.transform.tag == "WallTile") { }
                else { hit.collider.gameObject.GetComponent<TileLogic>().setNormalTile(); }
            }
            updateTileLists();
        }
    }

    void updateTileLists()
    {
        endPointList.Clear();
        startPointList.Clear();

        foreach (GameObject tile in gridList)
        {
            if (tile.GetComponent<TileLogic>().getStartStatus()) { startPointList.Add(tile); }
            else if (tile.GetComponent<TileLogic>().getEndStatus()) { endPointList.Add(tile); }
            else { }
        }

        if(endPointList.Count == 0 && startPointList.Count == 0)
        {
            if (spawnedPathFinder != null)
            { Destroy(spawnedPathFinder); }

            foreach (GameObject tile in gridList)
            {
                tile.GetComponent<TileLogic>().setTileG(0);
                tile.GetComponent<TileLogic>().setTileH(0);
                tile.GetComponent<TileLogic>().setHasDoneG(false);
                tile.GetComponent<TileLogic>().setHasDoneH(false);
                if(!tile.GetComponent<TileLogic>().checkWall())
                {
                    tile.GetComponent<SpriteRenderer>().color = Color.white;
                }
            }
        }
    }
    private List<GameObject> getNeighbours(int index)
    {
        List<GameObject> neighbourTiles = new List<GameObject>();

        if (index - 1 >= 0 && index - 1 < gridList.Count && gridList[index - 1].gameObject.tag != "WallTile")
        {
            if(gridList[index - 1].gameObject.transform.position.y != gridSize.y)
            {
                neighbourTiles.Add(gridList[index - 1]);
            }
        }
        if (index + 1 >= 0 && index + 1 < gridList.Count && gridList[index + 1].gameObject.tag != "WallTile")
        {
            if (gridList[index + 1].gameObject.transform.position.y != 1)
            {
                neighbourTiles.Add(gridList[index + 1]);
            }
            else { }
        }
        if (index - (int)gridSize.y >= 0 && index - (int)gridSize.y < gridList.Count && gridList[index - (int)gridSize.y].gameObject.tag != "WallTile")
        {
            neighbourTiles.Add(gridList[index - (int)gridSize.y]);
        }
        if (index + (int)gridSize.y >= 0 && index + (int)gridSize.y < gridList.Count && gridList[index + (int)gridSize.y].gameObject.tag != "WallTile")
        {
            neighbourTiles.Add(gridList[index + (int)gridSize.y]);
        }

        //Debug.Log("Passing neighbour list of size: " + neighbourTiles.Count);

        return neighbourTiles;
    }
}
