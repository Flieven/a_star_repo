﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinderLogic : MonoBehaviour
{
    [SerializeField] private GameObject startObject;
    [SerializeField] private GameObject endObject;
    [SerializeField] private GameObject currentObject;

    [SerializeField] private List<GameObject> openList = new List<GameObject>();
    [SerializeField] private List<GameObject> closedList = new List<GameObject>();

    [SerializeField] private List<GameObject> tileNeighbours = new List<GameObject>();

    private LineRenderer lRend = null;
    private GameObject lowestTile = null;

    [SerializeField] private float timeStep = 1.0f;
    private float nextTime = 0.0f;

    private void Start()
    {
        lowestTile = startObject;
        lRend = gameObject.GetComponent<LineRenderer>();

        currentObject = lowestTile;

        openList.Add(currentObject);

        CalcPath();
        nextTime = Time.time + timeStep;
    }

    private void Update()
    {
        foreach (GameObject tile in openList)
        {
            if (tile.GetComponent<TileLogic>().getStartStatus() == false && tile.GetComponent<TileLogic>().getEndStatus() == false)
            { tile.GetComponent<SpriteRenderer>().color = Color.green; }
        }
        foreach (GameObject tile in closedList)
        {
            if (tile.GetComponent<TileLogic>().getStartStatus() == false && tile.GetComponent<TileLogic>().getEndStatus() == false)
            { tile.GetComponent<SpriteRenderer>().color = Color.red; }
        }

        if (nextTime < Time.time)
        {
            CalcPath();
            nextTime = Time.time + timeStep;
        }

    }

    private void updateLineRenderer()
    {
        lRend.positionCount = closedList[1].GetComponent<TileLogic>().getHvalue();
        int Hvalue = closedList[closedList.Count - 1].GetComponent<TileLogic>().getHvalue();
        int lRendPos = 0;
        lRend.SetPosition(lRendPos, new Vector3(closedList[closedList.Count -1].transform.position.x, closedList[closedList.Count - 1].transform.position.y, -0.1f));
        lRendPos++;

        for(int i = closedList.Count -1; i >= 0; i--)
        {
            if(closedList[i].GetComponent<TileLogic>().getHvalue() > Hvalue)
            {
                lRend.SetPosition(lRendPos, new Vector3(closedList[i].transform.position.x, closedList[i].transform.position.y, -0.1f));
                Hvalue = closedList[i].GetComponent<TileLogic>().getHvalue();
                lRendPos++;
            }
        }
    }

    private void CalcPath()
    {
        //Debug.Log("Entered path calculation");

        currentObject = lowestTile;

        openList.Remove(currentObject);
        if(!closedList.Contains(currentObject)) { closedList.Add(currentObject); }

        if(currentObject == endObject) { Debug.Log("Found End!"); updateLineRenderer(); return; }

        else
        {
            currentObject.GetComponent<TileLogic>().setTileF(currentObject.GetComponent<TileLogic>().getGvalue() + currentObject.GetComponent<TileLogic>().getHvalue());

            setNeighbours();
            foreach(GameObject tile in tileNeighbours)
            {

                TileLogic tempLogic = tile.GetComponent<TileLogic>();

                tempLogic.setTileF(tempLogic.getGvalue() + tempLogic.getHvalue());

                if(closedList.Contains(tile)) { /*Debug.Log("Tile: " + tile.name + " Exists in closed list! Ignoring!");*/ }

                else if(!openList.Contains(tile))
                {
                    openList.Add(tile);
                }
            }

            foreach(GameObject tile in openList)
            {
                if(tile.GetComponent<TileLogic>().getFvalue() <= currentObject.GetComponent<TileLogic>().getFvalue() && tile.GetComponent<TileLogic>().getHvalue() < currentObject.GetComponent<TileLogic>().getHvalue())
                {
                    lowestTile = tile;
                    return;
                }
            }

            if(currentObject == lowestTile) { checkDeadEnds(); return; }

        }
    }

    private void checkDeadEnds()
    {
        Debug.LogWarning("We've reached a Dead End!");

        GameObject prevObj = closedList[closedList.Count - 2];

        foreach(GameObject tile in openList)
        {
            if (tile.GetComponent<TileLogic>().getFvalue() <= currentObject.GetComponent<TileLogic>().getFvalue() && tile.GetComponent<TileLogic>().getHvalue() <= currentObject.GetComponent<TileLogic>().getHvalue())
            {
                lowestTile = tile;
                closedList.Remove(currentObject);
                openList.Add(currentObject);
                return;
            }
            //else if(tile.GetComponent<TileLogic>().getHvalue() <= currentObject.GetComponent<TileLogic>().getHvalue())
            //{
            //    lowestTile = tile;
            //    return;
            //}
        }
    }

    private void setNeighbours()
    {
        //Debug.Log("Set neighbours");
        tileNeighbours = currentObject.GetComponent<TileLogic>().getNeighbours();
    }

    public void giveStartData(GameObject startPoint, GameObject endPoint)
    {
        startObject = startPoint;
        endObject = endPoint;
    }
}
