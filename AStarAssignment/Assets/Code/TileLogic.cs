﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileLogic : MonoBehaviour
{

    [Header("Start/End Point Data")]
    [SerializeField] private bool startPoint = false;
    [SerializeField] private Color startPointColor = Color.white;
    [SerializeField] private bool endPoint = false;
    [SerializeField] private Color endPointColor = Color.white;

    [Header("Tile Values")]
    [SerializeField] private int Hvalue = 0;
    [SerializeField] private int Gvalue = 0;
    [SerializeField] private int Fvalue = 0;
    [SerializeField] private int costModifier = 0;
    [SerializeField] private bool isWall = false;
    private bool hasCalculatedH = false;
    private bool hasCalculatedG = false;

    [Header("Utility")]
    [SerializeField] private GameObject textObject;
    [SerializeField] private TMPro.TextMeshPro txtMesh;

    private List<GameObject> myNeighbours = new List<GameObject>();

    private SpriteRenderer sRend = null;
    [SerializeField] private int indexPos = 0;


    private void Start()
    {
        sRend = gameObject.GetComponent<SpriteRenderer>();

        if(isWall) { sRend.color = Color.black; }

    }

    void Update()
    {
        txtMesh.text = "T: " +  transform.name + "\nG: " + Gvalue + "\nH: " + Hvalue + "\nF:" + Fvalue;
    }

    private IEnumerator calcGValues()
    {
        //Debug.Log("CalcGValues called on Tile: " + indexPos);
        int neighbourG = Gvalue + 1;

        if(myNeighbours.Count != 0)
        {
            for(int i = 0; i < myNeighbours.Count; i++)
            {
                //Debug.Log("Forloop progress: " + i);
                TileLogic tLogic = myNeighbours[i].GetComponent<TileLogic>();

                if (tLogic.getStartStatus() == false && tLogic.getGvalue() == 0)
                {
                    if (tLogic.checkWall() == false)
                    {
                        //Debug.Log("Tile is NOT wall!");

                        tLogic.setTileG(neighbourG);
                        //Debug.Log("Tile: " + tLogic.getIndex() + " Start Point = " + tLogic.getStartStatus() + " Value: " + tLogic.getGvalue());

                    }
                    else { /*Debug.Log("Tile IS wall..."); tLogic.setTileG(99);*/ }
                }
                else { /*Debug.Log("Tile has values!");*/ }
            }

            yield return null;

            hasCalculatedG = true;

            for (int i = 0; i < myNeighbours.Count; i++)
            {
                if(myNeighbours[i].GetComponent<TileLogic>().checkHasDoneG() == false && myNeighbours[i].GetComponent<TileLogic>().checkWall() == false)
                { myNeighbours[i].GetComponent<TileLogic>().ActivateCalcG(); }
            }
        }
    }

    private IEnumerator calcHValues()
    {
        int neighbourH = Hvalue + 1;

        if (myNeighbours.Count != 0)
        {
            for (int i = 0; i < myNeighbours.Count; i++)
            {
                TileLogic tLogic = myNeighbours[i].GetComponent<TileLogic>();

                if (tLogic.getStartStatus() == false && tLogic.getGvalue() == 0)
                {
                    if (tLogic.checkWall() == false)
                    {
                        if(tLogic.getEndStatus()) { tLogic.setTileH(0); }
                        else { tLogic.setTileH(neighbourH); }

                    }
                }
            }

            yield return null;

            hasCalculatedH = true;

            for (int i = 0; i < myNeighbours.Count; i++)
            {
                if (myNeighbours[i].GetComponent<TileLogic>().checkHasDoneH() == false && myNeighbours[i].GetComponent<TileLogic>().checkWall() == false)
                { myNeighbours[i].GetComponent<TileLogic>().ActivateCalcH(); }
            }
        }
    }

    public void setNormalTile()
    {
        startPoint = false;
        endPoint = false;
        sRend.color = Color.white;
        gameObject.tag = "GridTile";
    }
    public void setStartPos()
    {
        //Debug.Log("SetStart Start");
        startPoint = true;
        endPoint = false;
        sRend.color = startPointColor;
        Hvalue = 100;
        gameObject.tag = "StartTile";
        //Debug.Log("Before CalcG");
        StartCoroutine(calcGValues());
        //Debug.Log("After CalcG");
    }
    public void setEndPos()
    {
        startPoint = false;
        endPoint = true;
        sRend.color = endPointColor;
        gameObject.tag = "EndTile";
        StartCoroutine(calcHValues());
    }
    public bool getStartStatus() { return startPoint; }
    public bool getEndStatus()
    {
        return endPoint;
    }

    // Get | Set values
    #region
    public int getGvalue() { return Gvalue; }
    public int getHvalue() { return Hvalue; }
    public int getFvalue() { return Fvalue; }
    public void setTileG(int G)
    {
        Gvalue = G;
    }
    public void setTileH(int H)
    {
        Hvalue = H;
    }
    public void setTileF(int F) { Fvalue = F + costModifier; }
    public void setTileModifier(int M) { costModifier = M; }
    #endregion

    public void setIndex(int i)
    {
        indexPos = i;
    }
    public int getIndex() { return indexPos; }
    public void setNeighbours(List<GameObject> NList) { myNeighbours = NList; }
    public List<GameObject> getNeighbours() { return myNeighbours; }
    public void ActivateCalcG() { StartCoroutine(calcGValues()); }
    public void ActivateCalcH() { StartCoroutine(calcHValues()); }
    public bool checkWall() { return isWall; }
    public void setWall(bool value) { isWall = value; }
    public bool checkHasDoneG() { return hasCalculatedG; }
    public bool checkHasDoneH() { return hasCalculatedH; }

    public void setHasDoneG(bool value) { hasCalculatedG = value; }
    public void setHasDoneH(bool value) { hasCalculatedH = value; }
}
